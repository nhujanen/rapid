<?php

namespace Rapid;

class Request
{
    protected   $_type = null;
    protected   $_data = array();
    
    public function __construct($source)
    {
        switch ($source) {
            case 'post':
                $this->_type = INPUT_POST;
                break;
            case 'get':
                $this->_type = INPUT_GET;
                break;
        }
        
        return $this;        
    }
    
    public function __invoke($x) 
    {
    }
    
    public function __get($name) 
    {
        return filter_input($this->_type, $name);        
    }
}