<?php

namespace Rapid;

class Session
{
    protected   $_data = array();
    
    protected function __construct(array $data = array())
    {
        $this->_data = $data;
        
        return $this;
    }
    
    public static function get()
    {
        session_start();
        
        return new static($_SESSION);
    }
    
    public function __get($name) 
    {
        return array_key_exists($name, $this->_data) ? $this->_data[$name] : null;
    }
    
    public function __set($name, $value)
    {
        return ($this->_data[$name] = $value);
    }
    
    public function __isset($name) 
    {
        return array_key_exists($name, $this->_data);
    }
    
    public function __unset($name)
    {
        unset($this->_data[$name]);
    }
    
    public function __destruct() 
    {
        $_SESSION = $this->_data;
        session_write_close();
    }
}