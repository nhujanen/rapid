<?php

namespace Rapid;

class Database extends \PDO
{
    public function fetchColumn($query, array $values = array())
    {
        $smt = $this->prepare($query);
        $smt->execute($values);
        
        $data = $smt->fetchColumn(0);
        $smt->closeCursor();
        
        return $data;
    }
    
    public function fetchOne($query, array $values = array())
    {
        $smt = $this->prepare($query);
        $smt->execute($values);
        
        $data = $smt->fetch(static::FETCH_ASSOC);
        $smt->closeCursor();
        
        return $data;
    }
    
    public function fetchAll($query, array $values = array())
    {
        $smt = $this->prepare($query);
        $smt->execute($values);
        
        $data = $smt->fetchAll(static::FETCH_ASSOC);
        
        return $data;
    }
}