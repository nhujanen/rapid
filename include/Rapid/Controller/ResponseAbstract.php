<?php

namespace Rapid\Controller;

abstract class ResponseAbstract
{
    abstract public function process();
}