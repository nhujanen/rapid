<?php

namespace Rapid\Controller;

class Redirect extends ResponseAbstract
{
    protected   $_target    = null;
    
    public function __construct($target) 
    {
        $this->_target  = $target;
    }
    
    public function process() 
    {
        $_url = (strpos($this->_target, '//') === false) ?
            \Rapid::$base . $this->_target :
            $this->_target;
        
        header("Location: {$_url}");
        exit;
    }
}