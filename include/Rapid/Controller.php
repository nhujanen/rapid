<?php
namespace Rapid;

abstract class Controller
{
    protected   $_data = array();
    
    public static function toClass($value)
    {
        return preg_replace_callback(
            '/-(.)/', 
            function($hit) { 
                return strtoupper($hit[1]); 
            }, 
            ucfirst(strtolower(preg_replace('/[^a-z0-9_-]/', '', $value)))
        ) . 'Controller';
    }
    
    public static function toMethod($value)
    {
        return preg_replace_callback(
            '/-(.)/', 
            function($hit) { 
                return strtoupper($hit[1]); 
            }, 
            strtolower(preg_replace('/[^-a-z0-9_]/', '', $value))
        ) . 'Action';
    }
    
    public static function load($className)
    {
        if (!file_exists("app/controllers/{$className}.php")) {
            throw new Controller\Exception("Controller not found.", 404);
        }
        
        require_once "app/controllers/{$className}.php";
        
        return new $className;
    }
    
    public function call($methodName)
    {
        if (!method_exists($this, $methodName)) {
            throw new Controller\Exception("Method not found.", 404);
        }
        
        $return = call_user_func(array($this, $methodName));
        
        return $return;
    }
    
    public function __set($name, $value) 
    {
        return ($this->_data[$name] = $value);
    }
    
    public function __get($name) 
    {
        switch ($name) {
            case 'user':
                return \Rapid::$user;
            case 'post':
                return new \Rapid\Request('post');
            case 'get':
                return new \Rapid\Request('get');
        }
        return array_key_exists($name, $this->_data) ? $this->_data[$name] : null;
    }
    
    public function toArray()
    {
        return $this->_data;
    }
    
    public function go($target)
    {
        return new Controller\Redirect($target);
    }
    
    public function error($code)
    {
        return new Controller\Response($code);
    }
}