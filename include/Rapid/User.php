<?php

namespace Rapid;

class User
{
    protected   $_data = array();
    
    protected function __construct(array $data = array()) 
    {
        $this->_data = $data;
        
        return $this;
    }
    
    public static function get(Session $session)
    {
        if (isset($session->user)) {
            return static::load($session->user);
        } else {
            return false;
        }
    }
    
    public static function load($id)
    {
        $data = \Rapid::$db->fetchOne('SELECT * FROM users WHERE id = ? LIMIT 1', array(intval($id)));
        
        if (!$data) {
            throw new User\Exception("User not found.", 404);
        }
        
        return new static($data);
    }
    
    public static function login($username, $password)
    {
        $data = \Rapid::$db->fetchOne(
            'SELECT * FROM users WHERE username = ? AND password = ? LIMIT 1',
            array(
                $username,
                static::hash($password),
            )
        );
        
        if (!$data) {
            return false;
        } else {
            \Rapid::$session->user = $data['id'];
            return true;
        }
    }
    
    public function __get($name)
    {
        return array_key_exists($name, $this->_data) ? $this->_data[$name] : null;
    }
    
    public static function logout()
    {
        unset( \Rapid::$session->user );
        
        return true;
    }
    
    public static function hash($password)
    {
        return hash('sha512', $password);
    }
}