<?php

/**
 * Rapid core
 */

require_once 'Rapid/Exception.php';
require_once 'Rapid/Controller.php';
require_once 'Rapid/Session.php';

abstract class Rapid
{
    static  $cli            = false;
            
    static  $autoloader     = false;
    static  $base           = null;
    static  $uri            = null;
    
    static  $controller     = 'index';
    static  $action         = 'index';
    static  $format         = 'json';
    
    static  $session        = null;
    static  $user           = null;
    static  $db             = null;
    
    static  $config         = array(
        
    );
    
    /**
     * Add single or multiple paths to include_path.
     * 
     * @param string|array $paths
     */
    public static function register($paths)
    {
        if (!is_array($paths)) {
            $paths = array($paths);
        }

        set_include_path(
            get_include_path() .
            PATH_SEPARATOR . 
            implode(
                PATH_SEPARATOR,
                $paths
            )
        );
        
        if (!static::$autoloader) {
            spl_autoload_register(array('Rapid', 'load'));
            static::$autoloader = true;
        }
    }
    
    public static function load($className)
    {
        $fileName = str_replace(
            array('\\', '_'),
            DIRECTORY_SEPARATOR,
            $className
        );
        $fileName .= '.php';
        
        @include_once $fileName;
        if (!class_exists($className)) {
            throw new Exception("Class '{$className}' could not be found.", 500);
        }
    }
    
    public static function route()
    {
        if (static::$cli) return;
        
        static::$base       = ($_SERVER['SERVER_PORT'] == 443) ? 'https://' : 'http://';
        static::$base       .= $_SERVER['SERVER_NAME'];
        static::$base       .= dirname($_SERVER['SCRIPT_NAME']);
        static::$base       .= '/';

        $_uri               = substr($_SERVER['REQUEST_URI'], 0, (strpos($_SERVER['REQUEST_URI'], '?') !== false) ? strpos($_SERVER['REQUEST_URI'], '?') : strlen($_SERVER['REQUEST_URI']));
        static::$uri        = trim(substr($_uri, strlen(dirname($_SERVER['SCRIPT_NAME']))), '/');

        $_split             = array_filter(explode('.', static::$uri, 2));
        $_parts             = $_split ? array_filter(explode('/', $_split[0])) : array();
        
        static::$controller = array_key_exists(0, $_parts) ? $_parts[0] : 'index';
        static::$action     = array_key_exists(1, $_parts) ? $_parts[1] : 'index';
        static::$format     = array_key_exists(1, $_split) ? $_split[1] : 'json';
    }
    
    public static function configure()
    {
        static::$cli    = (php_sapi_name() === 'cli');
        
        if (file_exists('app/config.json')) {
            $configRaw      = file_get_contents('app/config.json');
            $configArray    = json_decode( $configRaw, true );
            if (!is_array($configArray)) {
                throw new \Rapid\Exception('Configuration file is malformed.', 500);
            }
            
            static::$config = array_replace_recursive(static::$config, $configArray);
            
            if (array_key_exists('database', static::$config)) {
                try {
                    static::$db = new \Rapid\Database(
                        array_key_exists('dsn', static::$config['database']) ? static::$config['database']['dsn'] : null,
                        array_key_exists('username', static::$config['database']) ? static::$config['database']['username'] : null,
                        array_key_exists('password', static::$config['database']) ? static::$config['database']['password'] : null,
                        array(
                            \PDO::MYSQL_ATTR_INIT_COMMAND   => "SET NAMES 'UTF8'",
                        )
                    );
                } catch (Exception $e) {
                    throw new \Rapid\Exception("No database.", 500);
                }
            }
        }
    }
    
    public static function execute()
    {
        if (static::$cli) {
            $commandName    = array_key_exists(2, $_SERVER['argv']) ? $_SERVER['argv'][2] : false;
            if (!$commandName) 
                throw new Exception('CLI Command not given.', 500);
            
            $commandFile    = strtolower($commandName) . '.php';
            if (!file_exists("app/cli/{$commandFile}"))
                throw new Exception('CLI Command not found', 404);
            
            require_once "app/cli/{$commandFile}";
            exit;
        }
        
        $className  = \Rapid\Controller::toClass(static::$controller);
        $actionName = \Rapid\Controller::toMethod(static::$action);
        
        $controller = \Rapid\Controller::load( $className );
        $response   = $controller->call($actionName);
        
        if ($response instanceof \Rapid\Controller\ResponseAbstract) {
            $response->process();
        } else if (is_array($response)) {
            foreach ($response as $_response) {
                if ($_response instanceof \Rapid\Controller\ResponseAbstract) {
                    $_response->process();
                }
            }
        }
        
        $data = $controller->toArray();
        
        switch (static::$format) {
            case 'json':
                echo json_encode($data);
                exit;
            case 'xml':
                $output = function(array $data, SimpleXMLElement $xml = null) use (&$output) {
                    $xml = is_null($xml) ? new SimpleXMLElement('<reply/>') : $xml;
                    foreach ($data as $key => $value) {
                        is_array($value) ?
                            $output($value, $xml->addChild($key)) :
                            $xml->addChild($key, $value);
                    }
                    return $xml;
                };
                echo $output($data)->asXML();
                exit;
            default:
                exit;
        }
    }
    
    public static function session()
    {
        if (static::$cli) return;
        
        static::$session    = \Rapid\Session::get();
        
        if (static::$db) {
            require_once 'app/User.php';
            try {
                static::$user       = User::get( static::$session );
            } catch (Exception $e) {
                static::$user       = false;
            }
        }
    }
}