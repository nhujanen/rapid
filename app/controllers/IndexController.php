<?php

class IndexController extends \Rapid\Controller
{
    public function indexAction()
    {
        if (!$this->user) {
            return $this->go('index/authorize');
        }
    }
    
    public function authorizeAction()
    {
        if ($this->user) {
            return $this->go('index');
        }
        
        if (User::login($this->get->uid, $this->get->pwd)) {
            return $this->go('index');
        } else {
            $this->error = true;
        }
    }
    
    public function logoutAction()
    {
        unset( \Rapid::$session->user );
        
        return $this->go('index');
    }
}