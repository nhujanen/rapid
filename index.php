<?php
/**
 * RAPID(K)
 * Rapid Application Programming Interface Development Kit
 * 
 * @author Niko Hujanen <niko.hujanen@gmail.com>
 * @copyright (c) 2016, Niko Hujanen
 * @version 1.00
 */

require_once 'include/Rapid.php';

try {
    
    \Rapid::register('include');
    
    \Rapid::configure();
    \Rapid::route();
    \Rapid::session();
    \Rapid::execute();        
    
} catch (\Rapid\Controller\Exception $e) {
    // Warning
    die($e->getMessage());
} catch (\Rapid\Exception $e) {
    // Critical
    die($e->getMessage());
} catch (PDOException $e) {
    // Fatal + silent
    die($e->getMessage());
} catch (Exception $e) {
    // Fatal
    die($e->getMessage());
}

